# Fig pre block. Keep at the top of this file.
[[ -f "$HOME/.fig/shell/zshrc.pre.zsh" ]] && . "$HOME/.fig/shell/zshrc.pre.zsh"
# ==========
# ==.zshrc==
# ==========

# personal prompt (DO NOT CHANGE FOR OTHER PLUGINS - MAKE ANOTHER SECTION)
# NEWLINE=$'\n'
# PROMPT="${NEWLINE}%F{red1}%~${NEWLINE}"
# # PS1=""
# RPROMPT='%D{%H:%M:%S}'


###########
# EVALS {{{
###########

command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
# starship (prompt configuration plugin)
eval "$(starship init zsh)"
# thefuck alias
eval $(thefuck --alias f)
# zoxide add to shell
eval "$(zoxide init zsh)"

# }}}

###########
# FUNCTIONS
###########

# Write a function to combine ls command with zoxide command
# function zl {
#   z "$@" && ls;
# }

function zl() { z "$@" && ;exa --icons --group-directories-first }

function cd {
    builtin cd "$@" && exa --icons --group-directories-first
    }

# starship terminal window title rename
function set_win_title(){
    echo -ne "\033]0; $(basename "$PWD") \007"
}
precmd_functions+=(set_win_title)





###########
# plugins=( [plugins...] zsh-syntax-highlighting)
###########
plugins=(vi-mode zsh-autosuggestions copyfile pep8)


# Settings {{{

# vi settings (unsure if needed)
VI_MODE_SET_CURSOR=true
VI_MODE_RESET_PROMPT_ON_MODE_CHANGE=true

## ==== FZF (fuzzy search)====

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# clang settings
CC="clang"
CFLAGS="-fsanitize=signed-integer-overflow -fsanitize=undefined -ggdb3 -O0 -std=c11 -Wall -Werror -Wextra -Wno-sign-compare -Wno-unused-parameter -Wno-unused-variable -Wshadow"
LDLIBS="-lcrypt -lcs50 -lm"


# }}}

# EXPORTS {{{
export ZSH=$HOME/.oh-my-zsh
export C_INCLUDE_PATH=/usr/local/include
export LD_LIBRARY_PATH=/usr/local/lib
export LIBRARY_PATH=/usr/local/lib
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export EDITOR=nvim

# }}}

# FUNCTIONS {{{

# }}}


# ALIASES {{{
alias copy='pbcopy < '
alias vimrc='nvim ~/.config/nvim'
# alias nvim='neovide'
alias tree='tree -C -L 2 -F --dirsfirst' # tree (ls alt) -L 1 limits maximum depth
alias ls='exa --icons --group-directories-first'  # exa (ls alternative)
alias lstree='exa --tree --level=2 --group-directories-first'
alias cat='bat --style=numbers,changes --theme=TwoDark'
alias cdf='cd $(find * -type d | fzf)'
alias py='python3.10 '
alias python='python3.10 '
alias rg='rg --auto-hybrid-regex'
alias z='zl'
# }}}

# PLUGIN SOURCING {{{

source /Users/aceserani/.oh-my-zsh/custom/plugins/copyfile/copyfile.plugin.zsh
source /Users/aceserani/.oh-my-zsh/custom/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
source /Users/aceserani/.oh-my-zsh/custom/plugins/vi-mode/vi-mode.plugin.zsh
source /Users/aceserani/.oh-my-zsh/custom/plugins/vi-mode/vi-mode.plugin.zsh

# must be at end of zshrc
source /Users/aceserani/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# }}}


test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# Fig post block. Keep at the bottom of this file.
[[ -f "$HOME/.fig/shell/zshrc.post.zsh" ]] && . "$HOME/.fig/shell/zshrc.post.zsh"
