augroup FileTests
autocmd BufEnter
  if &ft == "lua"
    echo "success!"
  elseif &ft == ".vimrc"
    echo "this is .vimrc"
  elseif &ft == "NvimTree"
    echo "TREEEEEE"
  else
    echo "failure"
  endif
augroup end

"
"
" "           =======================
" "           fennelcakes .vimrc file
" "           =======================
" "  test" "
"
" " FOR VIM ONLY -- DOES NOT MODIFY NVIM --{{{
" "}}}
"
"
" " Updated: (use <leader>D to populate datetime)
"  "Thu 2022-06-09 00:40:54-0600
"
" " (location: ~/.vimrc)
"
" " --- HOW TO SOURCE THE .vimrc file ---
" " :source ~/.vimrc (mapping: <leader>;sv )
"
" "            --- Fold Commands ---
" " `zo` to open a single fold under the cursor.
" " `zc` to close the fold under the cursor.
" " `zR` to open all folds.
" " `zM` to close all folds
" " `zf[motion]` to create a fold
"
"
" "
"
" " BASIC SETTINGS ---------------------------------------------------------- {{{
"
" " Automatic reloading of .vimrc
" " autocmd! StdinReadPre :source ~/.vim/.vimrc
" autocmd! bufwritepost :source ~/.vimrc
" " autocmd! bufwritepost ~/.vim/.vimrc source %
" " autocmd bufwritepost ~/.vimrc source $MYVIMRC
"
" " Set Colorscheme:
" syntax enable
" " Turn syntax highlighting on
" syntax on
" set termguicolors
"
" set cursorline
"
" " move cursor everywhere, regardless whether end of line is reached
" set virtualedit=all
" " confirm before closing unsaved file
" set confirm
"
" " Mouse Settings:
" set mouse=a     " on OSX press ALT and click
"
" set bs=2  " make backspace behave 'normal' (traverse lines)
"
"
" set hidden
"
"
" " Timeout for input
" set timeoutlen=250
"
"
" " Enable type file detection. Vim will be able to try to detect the type of file in use.
" filetype on
" " Enable plugins and load plugin for the detected file type.
" filetype plugin on
" " Load an indent file for the detected file type
" filetype indent on
" filetype plugin indent on
"
"
" " Line Displays:
" " Add numbers to each line on the left-hand side (number, relativenumber)
" set relativenumber
" " Showing line numbers and length
" set tw=79  " width of document (used by gd)
" set nowrap " don't automatically wrap on load
" set fo-=t  " don't automatically wrap text when typing
" set colorcolumn=80
" highlight ColorColumn ctermbg=233
"
"
" " Tab Handling:
" " Set shift width to 4 spaces
" set shiftwidth=4
" set tabstop=8
" set softtabstop=4
" set shiftround
" set autoindent
" " Use space characers instead of tabs
" set expandtab
"
"
" set nobackup      " Do not save backup file
" set nowritebackup
" set autoread
"
" " Do not let cursor scroll below or above N number of lines when scrolling
" set scrolloff=20
"
"
" " Search Case Settings:
" " While searching through a file incrementally highlight matching characters
" " as you type
" set incsearch
" " Ignore capital letters during search
" set ignorecase
" " Override the ignorecase option if searching for capital letters.
" " This will allow you to search specifically for capital letters.
" set smartcase
" " Show matching words during a search
" set showmatch
" " Use highlighting when doing a search
" set hlsearch
"
"
" " Command Related Settings:
" set showcmd  " Show partial command you tpe in the last line of the screen
" set showmode " Show the mode you are on on the last line.
" set cmdheight=2 " Give more space for displaying messages
"
"
" " Wildmenu:
" " Enable auto completion menu after pressing TAB.
" set wildmenu
" " Make wildmenu behave like similar to Bash completion.
" set wildmode=list:longest
" " Have wildmenu ignore these extenstions:
" set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx
"
"
" " Disable auto comment on new line
" autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" " autocmd BufNewFile,BufRead * setlocal formatoptions-=ro
"
"
" " }}}
"
"
" " PLUG CALLS{{{
"
" " --- ADDING PLUGINS ---
" " with Plug
" "   in PLUGINS fold, add 'Plug 'gitrepo''
" "   source .vimrc, then type ':PlugInstall'
"
" nnoremap <leader>pi :PlugInstall<CR>
" nnoremap <leader>pc :PlugClean<CR>
"
"
"
" call plug#begin('~/.vim/plugged')
"
"
" call plug#end()
"
" let g:textobj#quote#matchit = 1       " 0=disable, 1=enable (def)
"
"
" "}}}
"
"
" " VIMSCRIPT -------------------------------------------------------------- {{{
"
" " Folding: Use the marker method of folding.
" augroup filetype_vim
"     autocmd!
"     autocmd FileType vim setlocal foldmethod=marker
" augroup END
"
" " HTML Indentation: If the current file type is HTML, set indentation to 2 spaces.
" autocmd Filetype html setlocal tabstop=2 shiftwidth=2 expandtab
"
" " Undofile: If Vim version is equal to or greater than 7.3 enable undofile.
" " This allows you to undo changes to a file even after saving it.
" if version >= 703
"     set undodir=~/.vim/backup
"     set undofile
"     set undoreload=10000
" endif
"
" " Active Window Cursor: Display cursorline and cursorcolumn ONLY in active window.
" augroup cursor_off
"     autocmd!
"     autocmd WinLeave * set nocursorline nocursorcolumn
"     autocmd WinEnter * set cursorline cursorcolumn
" augroup END
"
" " GUI Settings:
" if has('gui_running')
"
"     " Mouse Settings:
"     set mouse=a     " on OSX press ALT and click
"     highlight Cursor guifg=white guibg=DarkCyan
"     " highlight iCursor guifg=white guibg=steelblue gui=bold
"     set guicursor=n-v-c:block-Cursor
"     " set guicursor+=i:ver100-iCursor
"     set guicursor+=n-v-c:blinkon0
"     set guicursor+=i:blinkwait10
"     set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50
"     \,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
"     \,sm:block-blinkwait175-blinkoff150-blinkon175c
"     " Font:
"     " Syntax: set guifont=<font_name>\ <font_weight>\ <size>
"     " set guifont=UbuntuMono\ Nerd\ Font\ Code:h16
"     " set guifont=MesloLGM\ NF\ Code:h16
"     set guifont=Source\ Code\ Pro\ Code:h16
"     " set guifont=Hack\ Nerd\ Font\ Code:h16
"
"     " Set Background Tone:
"     set background=dark
"
" endif
"
" " }}}
"
"
" " MAPPINGS --------------------------------------------------------------- {{{
"
" " CoC Diagnostics:
" " nnoremap <leader>dg :CocDiagnostics<CR>
"
" " Leader: leader to <space>
" nnoremap <SPACE> <Nop>
" let mapleader=" "
"
" " ShiftTab DeIndents:
" " for command mode
" nnoremap <S-Tab> <<
" " for insert mode
" inoremap <S-Tab> <C-d>
"
" " Fast Colon:<leader>; types the : character in command mode
" nnoremap <leader>; :
"
" " Smart Enter:
"
" "Write Quit And WriteQuit:
" nnoremap <leader>w :w<CR>
" nnoremap <leader>;q :q<CR>
" nnoremap <leader>;x :qa<CR>
"
"
" " Open Vimrc In Split Pane:
" nnoremap <silent> <Leader>;ve :e ~/.vimrc<CR>
" " Source Vimrc: <leader>;sv to reload .vimrc file
" nnoremap <leader>s :source ~/.vimrc <CR>
"
" " O Without Insert: Exit insert mode after creating a new line above or below the current line.
" nnoremap o o<esc>
" nnoremap O O<esc>
"
" " swap line/normal visual mode
" noremap V v
" noremap v V
"
" " Y: Yank (Y) from cursor to the end of line
" nnoremap Y y$
"
" " Paste Last Yank:
" nnoremap gp "0p
" nnoremap gP "0P
"
" " Copy Entire File To ClipBoard:
" nnoremap <leader>cat :%y+<CR>
"
" " E: E == ge (move to end of previous word)
" nnoremap E ge
"
" " Toggle Rnu And Nu:
" nnoremap <leader>rnu :set rnu!<CR>
" nnoremap <leader>nu :set nu!<CR>
"
"
"
" " Hide Search Highlight:
" nnoremap <leader>h :nohlsearch<CR>
"
" " Command Line History:
" nnoremap <leader>cmd q:
"
" " Terminal Command Shortcut: (trailing whitespace intentional)
" " to enter a command as if from a terminal (e.g. what F8 does to compile C)
" nnoremap <leader>;; :! 
"
" " F5 To Run Python:
" " :w saves file | <CR> (carriage return) | !clear runs external clear screen
" " command | !python3 % executes the current file.
" nnoremap <f4> :w <CR>:!clear <CR>:!python3 % <CR>
"
" " F5 To Compile And Run C:
" nnoremap <F5> : !gcc % && ./a.out <CR>
"
" " Window Mappings:
" " Split Windows: (horizontal and vertical)
" nnoremap <leader>ws <c-w>s
" nnoremap <leader>wv <c-w>v
" " Split Window Navigation Remaps:
" " Navigate split views with <ctrl-j><ctrl-k><ctrl-h><ctrl-l>
" nnoremap <leader>wj <c-w>j
" nnoremap <leader>wk <c-w>k
" nnoremap <leader>wh <c-w>h
" nnoremap <leader>wl <c-w>l
" nnoremap <leader>ww <c-w>w
" " Close Current Window:
" nnoremap <leader>wc <c-w>c
" " Close All Windows But Current:
" nnoremap <leader>wo <c-w>o
" " Equal Window Size:
" nnoremap <leader>w= <c-w>=
"
"
" " Tab Management:
" nnoremap <leader>tn :tabnew<CR>
" nnoremap <leader>tc :tabclose<CR>
" nnoremap <leader>to :tabonly<CR>
" " ':tabmove N', Move current tab to after tab page N.
" " User zero to make the current page the first one.
" " Without the N, the ta page is made the last one.
" " ':tabmove +/- N' Move N pages right of left
" "                           Whitespace intentional!!!!
" nnoremap <leader>tm :tabmove 
"
"
" " Yank Selection Visual Mode:
" vnoremap <leader>yc :'<,'>y+<CR>
"
" " Sort Function:  sort list alphabetically
" " vnoremap <leader>s :sort<CR>
"
" " Easy Code Block Movement: move selection with > and <
" vnoremap < <gv
" vnoremap > >gv
"
" " Print DateTime: (snippets does that for me now)
" " map <leader>D :put =strftime(' %a %Y-%m-%d %H:%M:%S%z')<CR>
"
" " Buffer Mappings:
" " Previous And Next Buffer: " use dot-command to keep cycling
" nnoremap <S-h> :bp<CR>
" nnoremap <S-l> :bn<CR>
" " Buffer Delete: Now being handle by Buffdelete
" " nnoremap <leader>bd :bd<CR>
"
"
" " Suspend To Terminal:
" " nnoremap <c-\> <c-z>
"
" " Replace under selection ATER cursor.
" " Need to add tofind/toreplace/g  (global optional)
" " e.g. v$:s/\%Vfoo/bar/g
" vnoremap <leader>vs :s/\%V
"
"
" " Pair Movement:
" " Move around pairs backwards
" function! MoveToPrevPairs()
"  "   Get line number and column
"     let backsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|'''
"     " Comment out above line and uncomment line below to include < >
"     " let backsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|''\|<\|>'
"     let [lnum, col] = searchpos(backsearch, 'bn')
" "    Move cursor
"     call setpos('.', [0, lnum, col, 0])
" endfunction
" " test line: ---(---)---[---]---{---}---`---`---'---'---"---"---<--->---
"
" " nnoremap <silent> <C-l> :call MoveToNextLink()<CR>
" nnoremap <silent> <C-k> <ESC>:call MoveToPrevPairs()<CR>
" inoremap <silent> <C-k> <ESC>:call MoveToPrevPairs()<CR>a
"
" " Move around pairs forward
" function! MoveToNextPairs()
"  "   Get line number and column
"     let forwardsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|'''
"     " Comment out above line and uncomment line below to include < >
"     " let forwardsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|''\|<\|>'
"     let [lnum, col] = searchpos(forwardsearch, 'n')
" "    Move cursor
"     call setpos('.', [0, lnum, col, 0])
" endfunction
"
" " nnoremap <silent> <C-l> :call MoveToNextLink()<CR>
" nnoremap <silent> <C-j> <ESC>:call MoveToNextPairs()<CR>
" inoremap <silent> <C-j> <ESC>:call MoveToNextPairs()<CR>a
" " test line: ---(---)---[---]---{---}---`---`---'---'---"---"---<--->---
"
" " }}}
"
"
" " LAST COMMANDS"{{{
"
"
"
" " VimCommentTitle Color: " gui sets 'attr-list'  (bold, underline, etc.)
" highlight vimCommentTitle guifg=DarkCyan gui=bold
"
"
"
" highlight Cursor guifg=white guibg=DarkCyan
" highlight iCursor guifg=white guibg=steelblue gui=bold
"
"
" set guicursor=n-v-c:block-Cursor
" set guicursor+=i:ver100-iCursor
"
" set guicursor+=i:blinkwait10
" set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50
" \,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
" \,sm:block-blinkwait175-blinkoff150-blinkon175c
"
"
"
" highlight Beacon guibg=White ctermbg=White
" highlight Beacon ctermbg=White ctermbg=White
"
" let g:beacon_size = 80
" let g:beacon_minimal_jump = 1
" let g:beacon_shrink = 0
" let g:beacon_timeout = 1000
"
"
"
" " Highlight cursor line underneath the cursor horionatally
" set cursorline
" " Highlight cursor line underneath the cursor vertically
" set cursorcolumn
"
" "}}}
