"---------------------------------------------------
"
"||\\     ||  \\          //  ||  ||\\          //||
"|| \\    ||   \\        //   ||  || \\        // ||
"||  \\   ||    \\      //    ||  ||  \\      //  ||
"||   \\  ||     \\    //     ||  ||   \\    //   ||
"||    \\ ||      \\  //      ||  ||    \\  //    ||
"||     \\||       \\//       ||  ||     \\//     ||
"
"---------------------------------------------------
"           ========================
"           fennelcakes .nvimrc file
"           ========================

" Updated: (use <leader>D to populate datetime)
" 2022-07-03 05:09:19 MDT

" (location: ~/.nvimrc)

" --- HOW TO SOURCE THE .nvimrc file ---
" :source ~/.nvimrc (mapping: <leader>;sv )

"            --- Fold Commands ---
" `zo` to open a single fold under the cursor.
" `zc` to close the fold under the cursor.
" `zR` to open all folds.
" `zM` to close all folds


"....known issues{{{

" 1 Jul 22:: fire-nvim not working in Firefox

" 16Jun22:: LSP still loads multiple times whenever .nvimrc is sourced.
" To get around this, I added the `:LspStop all` command to the source mapping,
" c.f. <leader>s mapping. Not the fix I ultimately want.

"}}}


" BASIC SETTINGS{{{

" Set Spellcheck:
augroup filetype_docs
  autocmd!
  autocmd FileType * setlocal spell
augroup END

" Set Colorscheme:
syntax enable
syntax on            " Turn syntax highlighting on
set termguicolors

set cursorline

" move cursor everywhere, regardless whether end of line is reached
" set virtualedit=all

set confirm         " confirm before closing unsaved file

" Mouse Settings:
set mouse=a         " on OSX press ALT and click

set bs=2  " make backspace behave 'normal' (traverse lines)


" TextEdit might fail if hidden is not set.
set hidden
" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Timeout for input
set timeoutlen=250


" Enable type file detection. Vim will be able to try to detect the type of file in use.
filetype on
" Enable plugins and load plugin for the detected file type.
filetype plugin on
" Load an indent file for the detected file type
filetype indent on
filetype plugin indent on


" Line Displays:
" Add numbers to each line on the left-hand side (number, relativenumber)
set relativenumber
" Showing line numbers and length
set tw=79  " width of document (used by gd)
set nowrap " don't automatically wrap on load
set fo-=t  " don't automatically wrap text when typing
set colorcolumn=80
highlight ColorColumn ctermbg=233


" Tab Handling:
" Set shift width to 4 spaces
set shiftwidth=4
set tabstop=8
set softtabstop=4
set shiftround
set autoindent
" Use space characters instead of tabs
set expandtab


set nobackup      " Do not save backup file
set nowritebackup
set autoread

" Do not let cursor scroll below or above N number of lines when scrolling
" set scrolloff=20


" Search Case Settings:
" While searching through a file incrementally highlight matching characters
" as you type
set incsearch
" Ignore capital letters during search
set ignorecase
" Override the ignorecase option if searching for capital letters.
" This will allow you to search specifically for capital letters.
set smartcase
" Show matching words during a search
set showmatch
" Use highlighting when doing a search
set hlsearch


" Command Related Settings:
set showcmd  " Show partial command you tpe in the last line of the screen
set showmode " Show the mode you are on on the last line.
set cmdheight=2 " Give more space for displaying messages


" Wildmenu:
" Enable auto completion menu after pressing TAB.
set wildmenu
" Make wildmenu behave like similar to Bash completion.
set wildmode=list:longest
" Have wildmenu ignore these extenstions:
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

set inccommand=split

" Disable auto comment on new line
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" autocmd BufNewFile,BufRead * setlocal formatoptions-=ro


"}}}


" PLUG CALLS{{{

" --- ADDING PLUGINS ---
" with Plug
"   in PLUGINS fold, add 'Plug 'gitrepo''
"   source .nvimrc, then type ':PlugInstall'

nnoremap <leader>pi :PlugInstall<CR>
nnoremap <leader>pc :PlugClean<CR>



call plug#begin('~/.vim/plugged')


"....aerial{{{

Plug 'stevearc/aerial.nvim'

"}}}

"....airline{{{

    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'enricobacis/vim-airline-clock'
    " let g:airline#extensions#clock#format = '%H:%M:%S'

"}}}

"....autopairs{{{

  Plug 'jiangmiao/auto-pairs'

"}}}

"....beacon{{{

Plug 'DanilaMihailov/beacon.nvim'

"}}}

"....better-escape{{{


Plug 'jdhao/better-escape.vim'



"}}}

"....better whitespace {{{

    Plug 'ntpeters/vim-better-whitespace'

"   To clean extra whitespace, call: :StripWhitespace
"   <leader>s to strip whitespace (e.g. <leader>sip strips inner paragraph
"   Change the mapping with:
"   let g:better_whitespace_operator='_s'

"}}}

"....bufdelete{{{

Plug 'famiu/bufdelete.nvim'

"}}}

"....catppuccin{{{

Plug 'catppuccin/nvim', {'as': 'catppuccin'}

"}}}

"....cheat-sheet{{{

    Plug 'Djancyp/cheat-sheet'

"}}}

"....clever-f {{{

    Plug 'rhysd/clever-f.vim'

"}}}

"....closetag{{{

Plug 'alvan/vim-closetag'

"}}}

"....cmp{{{

Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'SirVer/ultisnips'
Plug 'quangnguyen30192/cmp-nvim-ultisnips'

"}}}

"....colorizer{{{

Plug 'norcalli/nvim-colorizer.lua'

"}}}

"....commentary {{{

     Plug 'tpope/vim-commentary'

"}}}

"....ctrlsf{{{

Plug 'dyng/ctrlsf.vim'

"}}}

"....cursorline {{{

    Plug 'itchyny/vim-cursorword'

"}}}

"....devicons{{{

    Plug 'ryanoasis/vim-devicons'

"}}}

"....easy-align{{{

Plug 'junegunn/vim-easy-align'

"}}}

"....fugitive"{{{

    Plug 'tpope/vim-fugitive'

"}}}

"....goyo{{{

    Plug 'junegunn/goyo.vim'

"}}}

"....gruvbox-material{{{

Plug 'sainnhe/gruvbox-material'

"}}}

"....hop{{{

Plug 'phaazon/hop.nvim'

"}}}

"....inc-rename{{{

Plug 'smjonas/inc-rename.nvim'

"}}}

"....indent-blankline{{{

    Plug 'lukas-reineke/indent-blankline.nvim'

"}}}

"....jk-jumps{{{

" Handle movements with j/k as if they were real jumps
" This allows <ctrl-o> <ctrl-i> movement to include j/k moves
    Plug 'teranex/jk-jumps.vim'

"}}}

"....lightspeed{{{

" Plug 'ggandor/lightspeed.nvim'

"}}}

"....lsp management{{{

    Plug 'williamboman/nvim-lsp-installer'
    Plug 'neovim/nvim-lspconfig'
    Plug 'onsails/lspkind.nvim'
"}}}

"....lsp-saga{{{

Plug 'glepnir/lspsaga.nvim', { 'branch': 'main' }

"}}}

"....markdown-preview.nvim {{{

    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

"}}}

"....marks{{{

Plug 'chentoast/marks.nvim'

"}}}

"....NERDTree {{{

    Plug 'preservim/nerdtree'
    Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

" }}}

"....onedark {{{

    Plug 'navarasu/onedark.nvim'

"}}}

""....polyglot DISABLED{{{

"    Plug 'sheerun/vim-polyglot'

""}}}

"....prettier{{{

" post install (yarn install | npm install) then load plugin only for editing supported files
Plug 'prettier/vim-prettier', { 'do': 'npm install --frozen-lockfile --production' }

"}}}

"....quick-scope{{{

    Plug 'unblevable/quick-scope'

"}}}

"....rainbow{{{

    Plug 'frazrepo/vim-rainbow'

"}}}

"....repeat{{{

Plug 'tpope/vim-repeat'

"}}}

"....rigel theme{{{

Plug 'Rigellute/rigel'

"}}}

"....vim-snippets{{{

    Plug 'honza/vim-snippets'

"}}}

"....search-pulse{{{

Plug 'inside/vim-search-pulse'

"}}}

"....sort-folds {{{

    Plug 'obreitwi/vim-sort-folds'

"}}}

"....nvim-surround {{{

  Plug 'kylechui/nvim-surround'

"}}}

"....tablemode{{{

Plug 'dhruvasagar/vim-table-mode'

"}}}

"....telescope{{{

    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'nvim-telescope/telescope-fzy-native.nvim'

"}}}

"....todo-comments{{{

Plug 'folke/todo-comments.nvim'

"}}}

"....toggleterm{{{

Plug 'akinsho/toggleterm.nvim'

"}}}

"....treesitter{{{

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

"}}}

"....ufo{{{

Plug 'kevinhwang91/promise-async'
Plug 'kevinhwang91/nvim-ufo'

"}}}

"....undotree{{{

Plug 'mbbill/undotree'

"}}}

"....visual-multi{{{

    Plug 'mg979/vim-visual-multi'

"}}}

"....which-key{{{

   Plug 'folke/which-key.nvim'

"}}}

"....win(dow)resizer{{{

Plug 'simeji/winresizer'

"}}}

"....wilder{{{


if has('nvim')
  function! UpdateRemotePlugins(...)
    " Needed to refresh runtime files
    let &rtp=&rt
    UpdateRemotePlugins
  endfunction

  Plug 'gelguy/wilder.nvim', { 'do': function('UpdateRemotePlugins') }
else
  Plug 'gelguy/wilder.nvim'

  " To use Python remote plugin features in Vim, can be skipped
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif


    Plug 'nixprime/cpsm'

"}}}



call plug#end()

"}}}


" PLUGIN SETTINGS{{{{{{


"....aerial{{{

lua require('aerial').setup({})

nnoremap <leader>a :AerialToggle!<CR>

hi link AerialClass Type
hi link AerialClassIcon Special
hi link AerialFunction Special
hi AerialFunctionIcon guifg=#cb4b16 guibg=NONE guisp=NONE gui=NONE cterm=NONE

" There's also this group for the cursor position
hi link AerialLine QuickFixLine
" If highlight_mode="split_width", you can set a separate color for the
" non-current location highlight
hi AerialLineNC guibg=Gray

" You can customize the guides (if show_guide=true)
hi link AerialGuide Comment
" You can set a different guide color for each level
hi AerialGuide1 guifg=Red
hi AerialGuide2 guifg=Blue

"}}}

"....airline{{{

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_powerline_fonts = 1
let g:airline_theme='onedark'

"}}}

"....auto-pairs{{{
let g:AutoPairsShortcutFastWrap = '<C-f>'
" let g:AutoPairsMapCh = '<leader>dp'

"}}}

"....beacon{{{

" highlight Beacon guibg=Red ctermbg=Red
" highlight Beacon ctermbg=Red ctermbg=Red
" let g:beacon_size = 80
" let g:beacon_minimal_jump = 1
" let g:beacon_shrink = 0
" let g:beacon_timeout = 1000

"}}}

"....buffdelete{{{

nnoremap <leader>bd :Bdelete<CR>

"}}}

"....catppuccin

lua << EOF
local catppuccin = require("catppuccin")

-- catppuccin.setup(<settings>)
-- Lua
vim.g.catppuccin_flavour = "macchiato" -- latte, frappe, macchiato, mocha
vim.cmd[[colorscheme catppuccin]]
-- configure it
EOF



"....cheat-sheet{{{

    nnoremap <leader>ch :CheatSH<CR>

"}}}

"....clever-f {{{

let g:clever_f_smart_case = 1

" uncoment these if you want ; ' functionality back
" map ; <Plug>(clever-f-repeat-forward)
" map , <Plug>(clever-f-repeat-back)

"}}}

"....cmp{{

set completeopt=menu,menuone,noselect

lua <<EOF
  -- Setup nvim-cmp.
  local cmp = require'cmp'

  cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
        vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
      end,
    },
    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      -- { name = 'vsnip' }, -- For vsnip users.
      -- { name = 'luasnip' }, -- For luasnip users.
      { name = 'ultisnips' }, -- For ultisnips users.
      -- { name = 'snippy' }, -- For snippy users.
    }, {
      { name = 'buffer' },
    })
  })

  -- Set configuration for specific filetype.
  cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
      { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
    }, {
      { name = 'buffer' },
    })
  })

  -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
  --cmp.setup.cmdline('/', {
  --  mapping = cmp.mapping.preset.cmdline(),
  --  sources = {
  --    { name = 'buffer' }
  --  }
  --})

  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  --cmp.setup.cmdline(':', {
  --  mapping = cmp.mapping.preset.cmdline(),
  --  sources = cmp.config.sources({
  --    { name = 'path' }
  --  }, {
  --    { name = 'cmdline' }
  --  })
 -- })

  -- Setup lspconfig.
  local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
  -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
  --require('lspconfig')['<YOUR_LSP_SERVER>'].setup {
  --  capabilities = capabilities
  --}
EOF

""}}}

"....closetag{{{

" filenames like *.xml, *.html, *.xhtml, ...
" These are the file extensions where this plugin is enabled.
let g:closetag_filenames = '*.html,*.xhtml,*.phtml'

" filenames like *.xml, *.xhtml, ...
" This will make the list of non-closing tags self-closing in the specified files.
" let g:closetag_xhtml_filenames = '*.xhtml,*.jsx'

" filetypes like xml, html, xhtml, ...
" These are the file types where this plugin is enabled.
let g:closetag_filetypes = 'html,xhtml,phtml'

" filetypes like xml, xhtml, ...
" This will make the list of non-closing tags self-closing in the specified files.
" let g:closetag_xhtml_filetypes = 'xhtml,jsx'

" integer value [0|1]
" This will make the list of non-closing tags case-sensitive (e.g. `<Link>` will be closed while `<link>` won't.)
let g:closetag_emptyTags_caseSensitive = 1

" dict
" Disables auto-close if not in a "valid" region (based on filetype)
let g:closetag_regions = {
    \ 'typescript.tsx': 'jsxRegion,tsxRegion',
    \ 'javascript.jsx': 'jsxRegion',
    \ 'typescriptreact': 'jsxRegion,tsxRegion',
    \ 'javascriptreact': 'jsxRegion',
    \ }

" Shortcut for closing tags, default is '>'
let g:closetag_shortcut = '>'

" Add > at current position without closing the current tag, default is ''
let g:closetag_close_shortcut = '<leader>>'

"}}}

"....colorizer{{{

" DarkCyan
" #FF4499

" SETUP CALL FOR THIS NEEDS TO HAPPEN \AFTER\ colorscheme command below
" lua require'colorizer'.setup()


"}}}

"....ctrlsf{{{

nmap     <Leader><Leader>f <Plug>CtrlSFPrompt
vmap     <Leader><Leader>f <Plug>CtrlSFVwordPath
vmap     <Leader><Leader>F <Plug>CtrlSFVwordExec
nmap     <Leader><Leader>n <Plug>CtrlSFCwordPath
nmap     <Leader><Leader>p <Plug>CtrlSFPwordPath
nnoremap <Leader><Leader>o :CtrlSFOpen<CR>
nnoremap <Leader><Leader>t :CtrlSFToggle<CR>
inoremap <Leader><Leader>t <Esc>:CtrlSFToggle<CR>

"}}}

"....easy-align{{{

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

"}}}

"....goyo{{{

nnoremap <leader>go :Goyo<CR>
function! s:goyo_enter()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status off
    silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  endif
  set rnu
  set noshowmode
  set noshowcmd
  set scrolloff=999
  " ...
endfunction

function! s:goyo_leave()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status on
    silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  endif
  set showmode
  set showcmd
  set scrolloff=5
  " ...
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
"}}}

"....hop{{{

lua << EOF
require'hop'.setup()
-- place this in one of your configuration file(s)
--vim.api.nvim_set_keymap('', 'f', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true })<cr>", {})
--vim.api.nvim_set_keymap('', 'F', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true })<cr>", {})
--vim.api.nvim_set_keymap('', 't', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true, hint_offset = -1 })<cr>", {})
--vim.api.nvim_set_keymap('', 'T', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true, hint_offset = 1 })<cr>", {})
EOF


" Go to any word in the current buffer (:HopWord).
nnoremap ; :HopWord<CR>
onoremap ; :HopWord<CR>
" Go to any character in the current buffer (:HopChar1).
nnoremap s :HopChar1<CR>
" ys is a mapping for surround (e.g. ysiw' surrounds a word with '')
" Just use S as the onoremap for yank mode

" Go to any bigrams in the current buffer (:HopChar2).

" Make an arbitrary search akin to / and go to any occurrences (:HopPattern).
nnoremap S :HopPattern<CR>
onoremap S :HopPattern<CR>
vnoremap S :HopPattern<CR>
" Go to any line and any line start (:HopLine, :HopLineStart).
nnoremap T :HopLine<CR>
nnoremap t :HopLineStart<CR>

" Go to anywhere (:HopAnywhere).
" Use Hop cross windows with multi-windows support (:Hop*MW).
" Use it with commands like v, d, c, y to visually select/delete/change/yank up to your new cursor position.
" HopVertical
" Very similar to HopLine and HopLineStart, the only difference is that HopVertical will try to keep the cursor column position as the same as your current cursor.

"}}}

"....inc-rename{{{

lua << EOF
    require("inc_rename").setup()
vim.keymap.set("n", "<leader>rn", function()
  return ":IncRename " .. vim.fn.expand("<cword>")
end, { expr = true })

EOF

"}}}

"....indent-blankline{{{

let g:indentLine_color_term = 239

"}}}

"....jk-jumps{{{

" Handle movements with j/k as if they were real jumps
" This allows <ctrl-o> <ctrl-i> movement to include j/k moves
let g:jk_jumps_minimum_lines = 5

"}}}

""....lightspeed{{{

"map s <Plug>Lightspeed_omni_s
"map gs <Plug>Lightspeed_omni_gs

"lua << EOF
"require'lightspeed'.setup {
"ignore_case = true
"}

"EOF

""}}}

"....lsp config{{{

lua << EOF
require("nvim-lsp-installer").setup {}

local signs = { Error = " ", Warn = " ", Hint = " ", Info = "ﴞ " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end


EOF



"}}}

"....lsp-saga{{{

lua << EOF

local saga = require 'lspsaga'
saga.init_lsp_saga()

EOF
"-- lsp provider to find the cursor word definition and reference
" nnoremap <silent> gh <cmd>lua require'lspsaga.provider'.lsp_finder()<CR>
" -- or use command LspSagaFinder
nnoremap <silent> gh :Lspsaga lsp_finder<CR>

" -- code action
nnoremap <silent>ca <cmd>lua require('lspsaga.codeaction').code_action()<CR>
" vnoremap <silent><leader>ca :<C-U>lua require('lspsaga.codeaction').range_code_action()<CR>
" -- or use command
nnoremap <silent><leader>ca :Lspsaga code_action<CR>
vnoremap <silent><leader>ca :<C-U>Lspsaga range_code_action<CR>

" -- show hover doc
" nnoremap <silent> K <cmd>lua require('lspsaga.hover').render_hover_doc()<CR>
" -- or use command
nnoremap <silent>K :Lspsaga hover_doc<CR>

" -- scroll down hover doc or scroll in definition preview
nnoremap <silent> <C-f> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(1)<CR>
" -- scroll up hover doc
nnoremap <silent> <C-b> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(-1)<CR>

" -- show signature help
" nnoremap <silent> ghh <cmd>lua require('lspsaga.signaturehelp').signature_help()<CR>
" -- or command
nnoremap <silent> ghh :Lspsaga signature_help<CR>
" and you also can use smart_scroll_with_saga to scroll in signature help win

" -- rename
" nnoremap <silent>gr <cmd>lua require('lspsaga.rename').lsp_rename()<CR>
" -- or command
nnoremap <silent>gr :Lspsaga rename<CR>
" -- close rename win use <C-c> in insert mode or `q` in normal mode or `:q`

" -- preview definition
" nnoremap <silent> gd <cmd>lua require'lspsaga.provider'.preview_definition()<CR>
" " -- or use command
nnoremap <silent> gd :Lspsaga preview_definition<CR>
" can use smart_scroll_with_saga to scroll

" -- jump diagnostic
" nnoremap <silent> [e <cmd>lua require'lspsaga.diagnostic'.lsp_jump_diagnostic_prev()<CR>
" nnoremap <silent> ]e <cmd>lua require'lspsaga.diagnostic'.lsp_jump_diagnostic_next()<CR>
" -- or use command
nnoremap <silent> [e :Lspsaga diagnostic_jump_next<CR>
nnoremap <silent> ]e :Lspsaga diagnostic_jump_prev<CR>

" -- float terminal also you can pass the cli command in open_float_terminal function
" nnoremap <silent> <C-\> <cmd>lua require('lspsaga.floaterm').open_float_terminal()<CR>
" -- or open_float_terminal('lazygit')<CR>
" tnoremap <silent> <A-d> <C-\><C-n>:lua require('lspsaga.floaterm').close_float_terminal()<CR>
" -- or use command
" nnoremap <silent> <A-d> :Lspsaga open_floaterm<CR>
" tnoremap <silent> <A-d> <C-\><C-n>:Lspsaga close_floaterm<CR>

"}}}

"....lspkind{{{


" lua <<EOF
" local cmp = require('cmp')

" local lspkind = require('lspkind')

" formatting = {
"   format = lspkind.cmp_format({
"     mode = "symbol_text",
"     menu = ({
"       buffer = "[Buffer]",
"       nvim_lsp = "[LSP]",
"       luasnip = "[LuaSnip]",
"       nvim_lua = "[Lua]",
"       latex_symbols = "[Latex]",
"     })
"   }),
" }
" EOF

lua <<EOF
local kind_icons = {
  Text = "",
  Method = "",
  Function = "",
  Constructor = "",
  Field = "",
  Variable = "",
  Class = "ﴯ",
  Interface = "",
  Module = "",
  Property = "ﰠ",
  Unit = "",
  Value = "",
  Enum = "",
  Keyword = "",
  Snippet = "",
  Color = "",
  File = "",
  Reference = "",
  Folder = "",
  EnumMember = "",
  Constant = "",
  Struct = "",
  Event = "",
  Operator = "",
  TypeParameter = ""
}

local cmp = require('cmp')
cmp.setup {
  formatting = {
    format = function(entry, vim_item)
      -- Kind icons
      vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
      -- Source
      vim_item.menu = ({
        buffer = "[Buffer]",
        nvim_lsp = "[LSP]",
        luasnip = "[LuaSnip]",
        nvim_lua = "[Lua]",
        latex_symbols = "[LaTeX]",
      })[entry.source.name]
      return vim_item
    end
  },
}

EOF
"}}}

"....markdown-preview.nvim {{{


" Mappings:
nmap <leader>md <Plug>MarkdownPreviewToggle


" MarkdownPreview Config:
" set to 1, nvim will open the preview window after entering the markdown buffer
" default: 0
let g:mkdp_auto_start = 0

" set to 1, the nvim will auto close current preview window when change
" from markdown buffer to another buffer
" default: 1
let g:mkdp_auto_close = 1

" set to 1, the vim will refresh markdown when save the buffer or
" leave from insert mode, default 0 is auto refresh markdown as you edit or
" move the cursor
" default: 0
let g:mkdp_refresh_slow = 0

" set to 1, the MarkdownPreview command can be use for all files,
" by default it can be use in markdown file
" default: 0
let g:mkdp_command_for_global = 0

" set to 1, preview server available to others in your network
" by default, the server listens on localhost (127.0.0.1)
" default: 0
let g:mkdp_open_to_the_world = 0

" use custom IP to open preview page
" useful when you work in remote vim and preview on local browser
" more detail see: https://github.com/iamcco/markdown-preview.nvim/pull/9
" default empty
let g:mkdp_open_ip = ''

" specify browser to open preview page
" for path with space
" valid: `/path/with\ space/xxx`
" invalid: `/path/with\\ space/xxx`
" default: ''
let g:mkdp_browser = ''

" set to 1, echo preview page url in command line when open preview page
" default is 0
let g:mkdp_echo_preview_url = 0

" a custom vim function name to open preview page
" this function will receive url as param
" default is empty
let g:mkdp_browserfunc = ''

" options for markdown render
" mkit: markdown-it options for render
" katex: katex options for math
" uml: markdown-it-plantuml options
" maid: mermaid options
" disable_sync_scroll: if disable sync scroll, default 0
" sync_scroll_type: 'middle', 'top' or 'relative', default value is 'middle'
"   middle: mean the cursor position alway show at the middle of the preview page
"   top: mean the vim top viewport alway show at the top of the preview page
"   relative: mean the cursor position alway show at the relative positon of the preview page
" hide_yaml_meta: if hide yaml metadata, default is 1
" sequence_diagrams: js-sequence-diagrams options
" content_editable: if enable content editable for preview page, default: v:false
" disable_filename: if disable filename header for preview page, default: 0
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 1,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {},
    \ 'content_editable': v:false,
    \ 'disable_filename': 0,
    \ 'toc': {}
    \ }

" use a custom markdown style must be absolute path
" like '/Users/username/markdown.css' or expand('~/markdown.css')
let g:mkdp_markdown_css = ''

" use a custom highlight style must absolute path
" like '/Users/username/highlight.css' or expand('~/highlight.css')
let g:mkdp_highlight_css = ''

" use a custom port to start server or empty for random
let g:mkdp_port = ''

" preview page title
" ${name} will be replace with the file name
let g:mkdp_page_title = '「${name}」'

" recognized filetypes
" these filetypes will have MarkdownPreview... commands
let g:mkdp_filetypes = ['markdown']

" set default theme (dark or light)
" By default the theme is define according to the preferences of the system
let g:mkdp_theme = 'dark'

"}}}

"....marks{{{

lua << EOF
require'marks'.setup {
  -- whether to map keybinds or not. default true
  default_mappings = true,
  -- which builtin marks to show. default {}
  builtin_marks = { ".", "<", ">", "^" },
  -- whether movements cycle back to the beginning/end of buffer. default true
  cyclic = true,
  -- whether the shada file is updated after modifying uppercase marks. default false
  force_write_shada = false,
  -- how often (in ms) to redraw signs/recompute mark positions.
  -- higher values will have better performance but may cause visual lag,
  -- while lower values may cause performance penalties. default 150.
  refresh_interval = 250,
  -- sign priorities for each type of mark - builtin marks, uppercase marks, lowercase
  -- marks, and bookmarks.
  -- can be either a table with all/none of the keys, or a single number, in which case
  -- the priority applies to all marks.
  -- default 10.
  sign_priority = { lower=10, upper=15, builtin=8, bookmark=20 },
  -- disables mark tracking for specific filetypes. default {}
  excluded_filetypes = {},
  -- marks.nvim allows you to configure up to 10 bookmark groups, each with its own
  -- sign/virttext. Bookmarks can be used to group together positions and quickly move
  -- across multiple buffers. default sign is '!@#$%^&*()' (from 0 to 9), and
  -- default virt_text is "".
  bookmark_0 = {
    sign = "⚑",
    virt_text = "hello world"
  },
  mappings = {}
}
EOF

"}}}

"....NERDTree{{{

" Toggle NT
nnoremap <leader>e :NERDTreeToggle<CR>
nnoremap <leader>` :NERDTreeFocus<CR>
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
" Close the tab if NERDTree is the only window remaining in it.
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif
" enable line numbers
let NERDTreeShowLineNumbers=1
" make sure relative line numbers are used
autocmd FileType nerdtree setlocal relativenumber

"" }}}

"....onedark {{{
" dark, darker, cool, deep, warm, warmer, light

let g:onedark_config = {
  \ 'style': 'cool',
  \ 'toggle_style_key': '<leader>ts',
  \ 'ending_tildes': v:true,
  \ 'diagnostics': {
    \ 'darker': v:false,
    \ 'background': v:false,
  \ },
\ }

" *cterm-colors*{{{
" NR-16   NR-8    COLOR NAME 
" 0       0       Black
" 1       4       DarkBlue
" 2       2       DarkGreen
" 3       6       DarkCyan
" 4       1       DarkRed
" 5       5       DarkMagenta
" 6       3       Brown, DarkYellow
" 7       7       LightGray, LightGrey, Gray, Grey
" 8       0*      DarkGray, DarkGrey
" 9       4*      Blue, LightBlue
" 10      2*      Green, LightGreen
" 11      6*      Cyan, LightCyan
" 12      1*      Red, LightRed
" 13      5*      Magenta, LightMagenta
" 14      3*      Yellow, LightYellow
" 15      7*      White
"}}}

augroup MyColors
    autocmd!
    autocmd ColorScheme * highlight FoldColumn guibg=#21283B guifg=#455474
                      \ | highlight Search guibg=#455474 guifg=#FF5474
augroup END

colorscheme onedark
:lua package.loaded['colorizer'] = nil; require('colorizer').setup(...); require('colorizer').attach_to_buffer(0)

""}}}

"....rainbow{{{

let g:rainbow_active = 1

"}}}

"....sort-folds {{{

vmap <Leader>sf <Plug>SortFolds

let g:sort_folds_ignore_case = 1 " Ignore case when sorting

"}}}

"....nvim-surround{{{

lua << EOF
    require("nvim-surround").setup({
        -- Configuration here, or leave empty to use defaults
    })
EOF

"}}}

"....prettier{{{

nmap <Leader>, <Plug>(Prettier)

"}}}

"....tablemode{{{

nnoremap <Leader>table :TableModeToggle<CR>

function! s:isAtStartOfLine(mapping)
  let text_before_cursor = getline('.')[0 : col('.')-1]
  let mapping_pattern = '\V' . escape(a:mapping, '\')
  let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
  return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
endfunction

inoreabbrev <expr> <bar><bar>
          \ <SID>isAtStartOfLine('\|\|') ?
          \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
inoreabbrev <expr> __
          \ <SID>isAtStartOfLine('__') ?
          \ '<c-o>:silent! TableModeDisable<cr>' : '__'

"}}}

"....telescope{{{

lua << EOF
require('telescope').load_extension('fzy_native')
EOF

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>fc <cmd>Telescope command_history<cr>
nnoremap <leader>fr <cmd>Telescope oldfiles<cr>
nnoremap <leader>fa <cmd>Telescope grep_string<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>f' <cmd>Telescope marks<cr>
nnoremap <leader>f" <cmd>Telescope registers<cr>
nnoremap <leader>fk <cmd>Telescope keymaps<cr>
nnoremap <leader>f/ <cmd>Telescope current_buffer_fuzzy_find<cr>

" There's more cool LSP commands to bind! Check out the README and this fold below:

" telescope TO IMPLEMENT{{{

" Neovim LSP Pickers
" Functions 	Description
" builtin.lsp_references 	Lists LSP references for word under the cursor
" builtin.lsp_document_symbols 	Lists LSP document symbols in the current buffer
" builtin.lsp_workspace_symbols 	Lists LSP document symbols in the current workspace
" builtin.lsp_dynamic_workspace_symbols 	Dynamically Lists LSP for all workspace symbols
" builtin.diagnostics 	Lists Diagnostics for all open buffers or a specific buffer. Use option bufnr=0 for current buffer.
" builtin.lsp_implementations 	Goto the implementation of the word under the cursor if there's only one, otherwise show all options in Telescope
" builtin.lsp_definitions 	Goto the definition of the word under the cursor, if there's only one, otherwise show all options in Telescope
" builtin.lsp_type_definitions 	Goto the definition of the type of the word under the cursor, if there's only one, otherwise show all options in Telescope
" Git Pickers
" Functions 	Description
" builtin.git_commits 	Lists git commits with diff preview, checkout action <cr>, reset mixed <C-r>m, reset soft <C-r>s and reset hard <C-r>h
" builtin.git_bcommits 	Lists buffer's git commits with diff preview and checks them out on <cr>
" builtin.git_branches 	Lists all branches with log preview, checkout action <cr>, track action <C-t> and rebase action<C-r>
" builtin.git_status 	Lists current changes per file with diff preview and add action. (Multi-selection still WIP)
" builtin.git_stash 	Lists stash items in current repository with ability to apply them on <cr>




"}}}



"}}}

"....todo-comments{{{

lua << EOF
  require("todo-comments").setup {
    -- your configuration comes here
    -- or leave it empty to use the default settings
    -- refer to the configuration section below

{
  signs = true, -- show icons in the signs column
  sign_priority = 8, -- sign priority
  -- keywords recognized as todo comments
  keywords = {
    FIX = {
      icon = " ", -- icon used for the sign, and in search results
      color = "error", -- can be a hex color, or a named color (see below)
      alt = { "FIXME", "BUG", "FIXIT", "ISSUE" }, -- a set of other keywords that all map to this FIX keywords
      -- signs = false, -- configure signs for some keywords individually
    },
    TODO = { icon = " ", color = "info" },
    HACK = { icon = " ", color = "warning" },
    WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
    PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
    NOTE = { icon = " ", color = "hint", alt = { "INFO" } },
  },
  merge_keywords = true, -- when true, custom keywords will be merged with the defaults
  -- highlighting of the line containing the todo comment
  -- * before: highlights before the keyword (typically comment characters)
  -- * keyword: highlights of the keyword
  -- * after: highlights after the keyword (todo text)
  highlight = {
    before = "", -- "fg" or "bg" or empty
    keyword = "wide", -- "fg", "bg", "wide" or empty. (wide is the same as bg, but will also highlight surrounding characters)
    after = "fg", -- "fg" or "bg" or empty
    pattern = [[.*<(KEYWORDS)\s*:]], -- pattern or table of patterns, used for highlightng (vim regex)
    comments_only = true, -- uses treesitter to match keywords in comments only
    max_line_len = 400, -- ignore lines longer than this
    exclude = {}, -- list of file types to exclude highlighting
  },
  -- list of named colors where we try to extract the guifg from the
  -- list of hilight groups or use the hex color if hl not found as a fallback
  colors = {
    error = { "DiagnosticError", "ErrorMsg", "#DC2626" },
    warning = { "DiagnosticWarning", "WarningMsg", "#FBBF24" },
    info = { "DiagnosticInfo", "#2563EB" },
    hint = { "DiagnosticHint", "#10B981" },
    default = { "Identifier", "#7C3AED" },
  },
  search = {
    command = "rg",
    args = {
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
    },
    -- regex that will be used to match keywords.
    -- don't replace the (KEYWORDS) placeholder
    pattern = [[\b(KEYWORDS):]], -- ripgrep regex
    -- pattern = [[\b(KEYWORDS)\b]], -- match without the extra colon. You'll likely get false positives
  },
}

  }
EOF

"}}}

"....toggleterm{{{

lua << EOF

require("toggleterm").setup{
  -- size can be a number or function which is passed the current terminal
--  size = 20 | function(term)
--    if term.direction == "horizontal" then
--      return 15
--    elseif term.direction == "vertical" then
--      return vim.o.columns * 0.4
--    end
--  end,
  open_mapping = [[<c-\>]],
  hide_numbers = true, -- hide the number column in toggleterm buffers
--  shade_filetypes = {},
--  highlights = {
--    -- highlights which map to a highlight group name and a table of it's values
--    -- NOTE: this is only a subset of values, any group placed here will be set for the terminal window split
--    Normal = {
--      guibg = "<VALUE-HERE>",
--    },
--    NormalFloat = {
--      link = 'Normal'
--    },
--    FloatBorder = {
--      guifg = "<VALUE-HERE>",
--      guibg = "<VALUE-HERE>",
--    },
--  },
  shade_terminals = true, -- NOTE: this option takes priority over highlights specified so if you specify Normal highlights you should set this to false
--  shading_factor = '<number>', -- the degree by which to darken to terminal colour, default: 1 for dark backgrounds, 3 for light
  start_in_insert = false,
  insert_mappings = true, -- whether or not the open mapping applies in insert mode
  terminal_mappings = true, -- whether or not the open mapping applies in the opened terminals
  persist_size = true,
  persist_mode = true, -- if set to true (default) the previous terminal mode will be remembered
  direction = 'vertical', --'vertical' | 'horizontal' | 'tab' | 'float',
  close_on_exit = true, -- close the terminal window when the process exits
  shell = vim.o.shell, -- change the default shell
  -- This field is only relevant if direction is set to 'float'
  float_opts = {
    -- The border key is *almost* the same as 'nvim_open_win'
    -- see :h nvim_open_win for details on borders however
    -- the 'curved' border is a custom border type
    -- not natively supported but implemented in this plugin.
    border = 'shadow', -- 'single' | 'double' | 'shadow' | 'curved' | ... other options supported by win open
--    width = <value>,
--    height = <value>,
    winblend = 3,
  }
}

EOF

"}}}

"....treesitter{{{

lua <<EOF
require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all"
  ensure_installed = { "c", "lua", "python", "html", "css", "vim", "javascript" },
  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- List of parsers to ignore installing (for "all")
  ignore_install = {},

  highlight = {
    -- `false` will disable the whole extension
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    disable = {},

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = true,
  },
}
EOF

"}}}

"....ufo{{{

lua << EOF

-- option 2: nvim lsp as LSP client
-- tell the sever the capability of foldingRange
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.foldingRange = {
    dynamicRegistration = false,
    lineFoldingOnly = true
}
--

require('ufo').setup()

EOF

"}}}

"....undotree

nnoremap <Leader>u :UndotreeToggle<CR>

if has("persistent_undo")
   let target_path = expand('~/.undodir')

    " create the directory and any parent directories
    " if the location does not exist.
    if !isdirectory(target_path)
        call mkdir(target_path, "p", 0700)
    endif

    let &undodir=target_path
    set undofile
endif

"....which-key {{{


lua << EOF
  require("which-key").setup {
    plugins = {
        marks = true, -- shows a list of your marks on ' and `
        registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
        spelling = {
            enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
            suggestions = 20, -- how many suggestions should be shown in the list?
        },
        -- the presets plugin, adds help for a bunch of default keybindings in Neovim
        -- No actual key bindings are created
        presets = {
            operators = false, -- adds help for operators like d, y, ... and registers them for motion / text object completion
            motions = true, -- adds help for motions
            text_objects = true, -- help for text objects triggered after entering an operator
            windows = true, -- default bindings on <c-w>
            nav = true, -- misc bindings to work with windows
            z = true, -- bindings for folds, spelling and others prefixed with z
            g = true, -- bindings for prefixed with g
        },
    },
    -- add operators that will trigger motion and text object completion
    -- to enable all native operators, set the preset / operators plugin above
    operators = { gc = "Comments" },
    key_labels = {
        -- override the label used to display some keys. It doesn't effect WK in any other way.
        -- For example:
        -- ["<space>"] = "SPC",
        -- ["<cr>"] = "RET",
        -- ["<tab>"] = "TAB",
    },
    icons = {
        breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
        separator = "➜", -- symbol used between a key and it's label
        group = "+", -- symbol prepended to a group
    },
    popup_mappings = {
        scroll_down = '<c-d>', -- binding to scroll down inside the popup
        scroll_up = '<c-u>', -- binding to scroll up inside the popup
    },
    window = {
        border = "none", -- none, single, double, shadow
        position = "bottom", -- bottom, top
        margin = { 1, 0, 1, 0 }, -- extra window margin [top, right, bottom, left]
        padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
        winblend = 0
    },
    layout = {
        height = { min = 4, max = 25 }, -- min and max height of the columns
        width = { min = 20, max = 50 }, -- min and max width of the columns
        spacing = 3, -- spacing between columns
        align = "left", -- align columns left, center or right
    },
    ignore_missing = false, -- enable this to hide mappings for which you didn't specify a label
    hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ "}, -- hide mapping boilerplate
    show_help = true, -- show help message on the command line when the popup is visible
    triggers = "auto", -- automatically setup triggers
    -- triggers = {"<leader>"} -- or specify a list manually
    triggers_blacklist = {
        -- list of mode / prefixes that should never be hooked by WhichKey
        -- this is mostly relevant for key maps that start with a native binding
        -- most people should not need to change this
        i = { "j", "k" },
        v = { "j", "k" },
    },
  }
EOF

nnoremap <leader>km :WhichKey<CR>

"}}}

"....wilder{{{

" Default keys
call wilder#setup({
      \ 'modes': [':', '/', '?'],
      \ 'next_key': '<Tab>',
      \ 'previous_key': '<S-Tab>',
      \ 'accept_key': '<Down>',
      \ 'reject_key': '<Up>',
      \ })

call wilder#set_option('pipeline', [
      \   wilder#branch(
      \     wilder#cmdline_pipeline({
      \       'language': 'python',
      \       'fuzzy': 1,
      \     }),
      \     wilder#python_search_pipeline({
      \       'pattern': wilder#python_fuzzy_pattern(),
      \       'sorter': wilder#python_difflib_sorter(),
      \       'engine': 're',
      \     }),
      \   ),
      \ ])

call wilder#set_option('renderer', wilder#popupmenu_renderer({
      \ 'border': 'rounded',
      \ 'highlighter': wilder#basic_highlighter(),
      \ 'highlights': {
      \   'accent': wilder#make_hl('WilderAccent', 'Pmenu', [{}, {}, {'foreground': '#f4468f'}]),
      \ },
      \ 'left': [
      \   ' ', wilder#popupmenu_devicons(),
      \ ],
      \ 'right': [
      \   ' ', wilder#popupmenu_scrollbar(),
      \ ],
      \ }))


"}}}



"}}}}}}


" VIMSCRIPT {{{

" Folding: Use the marker method of folding.
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" Enable Fold Column:
set foldcolumn=1


" HTML Indentation: If the current file type is HTML, set indentation to 2 spaces.
autocmd Filetype html setlocal tabstop=2 shiftwidth=2 expandtab

" Undofile: If Vim version is equal to or greater than 7.3 enable undofile.
" This allows you to undo changes to a file even after saving it.
if version >= 703
    set undodir=~/.vim/backup
    set undofile
    set undoreload=10000
endif

" Active Window Cursor: Display cursorline and cursorcolumn ONLY in active window.
augroup cursor_off
    autocmd!
    autocmd WinLeave * set nocursorline nocursorcolumn
    autocmd WinEnter * set cursorline cursorcolumn
augroup END

" GUI Settings:
if has('gui_running')

    " Mouse Settings:
    set mouse=a     " on OSX press ALT and click
    highlight Cursor guifg=White guibg=DarkCyan
    highlight iCursor guifg=White guibg=SteelBlue gui=bold
    set guicursor=n-v-c:block-Cursor
    " set guicursor+=i:ver100-iCursor
    set guicursor+=n-v-c:blinkon0
    set guicursor+=i:blinkwait10
    set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50
    \,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
    \,sm:block-blinkwait175-blinkoff150-blinkon175c
    " Font:
    " Syntax: set guifont=<font_name>\ <font_weight>\ <size>
    " set guifont=UbuntuMono\ Nerd\ Font\ Code:h16
    " set guifont=MesloLGM\ NF\ Code:h16
    " set guifont=Source\ Code\ Pro\ Code:h16
    set guifont=Sauce\ Code\ Pro\ Nerd\ Font\ Complete\ Code:h16
    " set guifont=Hack\ Nerd\ Font\ Code:h16

    " Set Background Tone:
    set background=dark

endif

" }}}


" MAPPINGS {{{

" Leader: leader to <space>
nnoremap <SPACE> <Nop>
let mapleader=" "

" Fast EntireFile Substitution Cmd:
nnoremap <leader>;sg :%s///g<Left><Left><Left>
" Fast CurrentLine To FinalLine Substitution Cmd:
nnoremap <leader>;sl :.,$s///g<Left><Left><Left>

" Set Colorscheme:
nnoremap <leader>cs :colorscheme 

" Toggle DarkLight Mode:
function ToggleBackgroundColour ()
    if (&background == 'light')
        set background=dark
        echo "background -> dark"
    else
        set background=light
        echo "background -> light"
    endif
endfunction

nnoremap <leader>tbg :call ToggleBackgroundColour()<CR>

" Set Gruvbox Light:
nnoremap <leader>gbl :colorscheme gruvbox<CR> :call ToggleBackgroundColour()<CR>


" ShiftTab DeIndents:
" for command mode
nnoremap <S-Tab> <<
" for insert mode
inoremap <S-Tab> <C-d>

" Fast Colon:<leader>; types the : character in command mode
nnoremap <leader>; :

" Insert Mode Default New Line:
inoremap <c-o><c-o> <esc>$o

"Write Quit And WriteQuit:
nnoremap <leader>w :w<CR>
nnoremap <leader>;q :q<CR>
nnoremap <leader>;x :x<CR>

"Toggle Spellcheck:
nnoremap <leader>sp :set spell!<CR>


" Open nvimrc In Split Pane:
nnoremap <silent> <Leader>;ve :e ~/.nvimrc<CR>
" Source nvimrc: <leader>s to reload .vimrc file
nnoremap <leader>s :source ~/.nvimrc<CR>:LspStop all<CR>

" O Without Insert: Exit insert mode after creating a new line above or below the current line.
nnoremap o o<esc>
nnoremap O O<esc>

" Swap Line And Normal Visual Mode:
noremap V v
noremap v V

" Y: Yank (Y) from cursor to the end of line
nnoremap Y y$

" Paste Last Yank:
nnoremap gp "0p
nnoremap gP "0P

" Copy Entire File To ClipBoard:
nnoremap <leader>cat :%y+<CR>

" E: E == ge (move to end of previous word)
nnoremap E ge

" Toggle Rnu And Nu:
nnoremap <leader>rnu :set rnu!<CR>
nnoremap <leader>nu :set nu!<CR>


" Hide Search Highlight:
nnoremap <leader>h :nohlsearch<CR>

" Command Line History:
nnoremap <leader>cmd q:

" Terminal Command Shortcut: (trailing whitespace intentional)
" to enter a command as if from a terminal (e.g. what F8 does to compile C)
nnoremap <leader>;; :! 

" F5 To Run Python:
" :w saves file | <CR> (carriage return) | !clear runs external clear screen
" command | !python3 % executes the current file.
nnoremap <f4> :w <CR>:!clear <CR>:!python3 % <CR>

" F5 To Compile And Run C:
nnoremap <F5> : !gcc % && ./a.out <CR>

" Window Mappings:
" Split Windows: (horizontal and vertical)
nnoremap <leader>ws <c-w>s
nnoremap <leader>wv <c-w>v
" Split Window Navigation Remaps:
" Navigate split views with <ctrl-j><ctrl-k><ctrl-h><ctrl-l>
nnoremap <leader>wj <c-w>j
nnoremap <leader>wk <c-w>k
nnoremap <leader>wh <c-w>h
nnoremap <leader>wl <c-w>l
nnoremap <leader>ww <c-w>w
" Close Current Window:
nnoremap <leader>wc <c-w>c
" Close All Windows But Current:
nnoremap <leader>wo <c-w>o
" Equal Window Size:
nnoremap <leader>w= <c-w>=


" Tab Management:
nnoremap <leader>tn :tabnew<CR>
nnoremap <leader>tc :tabclose<CR>
nnoremap <leader>to :tabonly<CR>
" ':tabmove N', Move current tab to after tab page N.
" User zero to make the current page the first one.
" Without the N, the ta page is made the last one.
" ':tabmove +/- N' Move N pages right of left
"                           Whitespace intentional!!!!
nnoremap <leader>tm :tabmove 


" Replace Pattern In Visual Mode:
" Need to add tofind/toreplace/g  (global optional)
" e.g. v$:s/\%Vfoo/bar/g
vnoremap <leader>vs :s/\v
" Delete Pattern In Visual Mode:
vnoremap <leader>vd :s///<Left><Left>

" Yank Selection Visual Mode:
vnoremap <leader>yc :'<,'>y+<CR>

" Sort Function:  sort list alphabetically
" vnoremap <leader>s :sort<CR>

" Easy Code Block Movement: move selection with > and <
vnoremap < <gv
vnoremap > >gv

" Print DateTime: (snippets does that for me now)
" map <leader>D :put =strftime(' %a %Y-%m-%d %H:%M:%S%z')<CR>

" Buffer Mappings:
" Previous And Next Buffer: " use dot-command to keep cycling
nnoremap <S-h> :bp<CR>
nnoremap <S-l> :bn<CR>
" Buffer Delete: Now being handle by Buffdelete
" nnoremap <leader>bd :bd<CR>


" Delete Trailing Whitespace From Visual Selection:
vnoremap <leader>rw :s/\s*$//<CR>/Removed trailing whitespace!<CR>

" Suspend To Terminal:
" nnoremap <c-\> <c-z>


" Pair Movement:
" Move around pairs backwards
function! MoveToPrevPairs()
 "   Get line number and column
    " let backsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|'''
    " Comment out above line and uncomment line below to include < >
    let backsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|''\|<\|>'
    let [lnum, col] = searchpos(backsearch, 'bn')
"    Move cursor
    call setpos('.', [0, lnum, col, 0])
endfunction
" test line: ---(---)---[---]---{---}---`---`---'---'---"---"---<--->---

" nnoremap <silent> <C-l> :call MoveToNextLink()<CR>
nnoremap <silent> <C-k> <ESC>:call MoveToPrevPairs()<CR>
inoremap <silent> <C-k> <ESC>:call MoveToPrevPairs()<CR>a

" Move around pairs forward
function! MoveToNextPairs()
 "   Get line number and column
    " let forwardsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|'''
    " Comment out above line and uncomment line below to include < >
    let forwardsearch = '(\|)\|\[\|\]\|{\|}\|"\|`\|''\|<\|>'
    let [lnum, col] = searchpos(forwardsearch, 'n')
"    Move cursor
    call setpos('.', [0, lnum, col, 0])
endfunction

" nnoremap <silent> <C-l> :call MoveToNextLink()<CR>
nnoremap <silent> <C-j> <ESC>:call MoveToNextPairs()<CR>
inoremap <silent> <C-j> <ESC>:call MoveToNextPairs()<CR>a
" test line: ---(---)---[---]---{---}---`---`---'---'---"---"---<--->---


" tpope's repeat: NEEDS TO BE AT END OF MAPPINGS
silent! call repeat#set("\<Plug>MyWonderfulMap", v:count)

" }}}


" LAST COMMANDS"{{{


" ...lsp configs


lua << EOF
  require('lspconfig')['clangd'].setup {
    capabilities = capabilities,
    on_attach = require("aerial").on_attach
  }
--  require('lspconfig')['cssls'].setup {
--    capabilities = capabilities,
--    on_attach = require("aerial").on_attach
--  }
  require('lspconfig')['denols'].setup {
    capabilities = capabilities,
    on_attach = require("aerial").on_attach
  }
  require('lspconfig')['emmet_ls'].setup {
    capabilities = capabilities,
    on_attach = require("aerial").on_attach
  }
  require('lspconfig')['eslint'].setup {
    capabilities = capabilities,
    on_attach = require("aerial").on_attach
  }
  require('lspconfig')['jsonls'].setup {
    capabilities = capabilities,
    on_attach = require("aerial").on_attach
  }
--  require('lspconfig')['marksman'].setup {
--    capabilities = capabilities,
--    on_attach = require("aerial").on_attach
--  }
  require('lspconfig')['pyright'].setup {
    capabilities = capabilities,
    on_attach = require("aerial").on_attach
  }
  require('lspconfig')['sqls'].setup {
    capabilities = capabilities,
    on_attach = require("aerial").on_attach
  }
  require('lspconfig')['sumneko_lua'].setup {
    capabilities = capabilities,
    on_attach = require("aerial").on_attach
  }
  require('lspconfig')['vimls'].setup {
    capabilities = capabilities,
    on_attach = require("aerial").on_attach
  }

EOF


" VimCommentTitle Color: " gui sets 'attr-list'  (bold, underline, etc.)
highlight vimCommentTitle guifg=DarkCyan gui=bold

" Line Number Color:
" highlight LineNr guifg=#EFBD5D

" Cursor Appearance:
highlight Cursor guifg=white guibg=DarkCyan
highlight iCursor guifg=white guibg=steelblue gui=bold


set guicursor=n-v-c:block-Cursor
set guicursor+=i:ver100-iCursor

set guicursor+=i:blinkwait10
set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50
\,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
\,sm:block-blinkwait175-blinkoff150-blinkon175c


" Beacon Appearance:
" Putting it in alphabetical order in the Plug Settings section didn't
" change these
highlight Beacon guibg=White ctermbg=White
highlight Beacon ctermbg=White ctermbg=White

let g:beacon_size = 80
let g:beacon_minimal_jump = 1
let g:beacon_shrink = 0
let g:beacon_timeout = 1000


" Highlight Cursor Lines:
" Highlight cursor line underneath the cursor horionatally
set cursorline
" Highlight cursor line underneath the cursor vertically
set cursorcolumn


"}}}
